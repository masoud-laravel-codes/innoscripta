<?php

namespace App\Models;

use App\Services\NewsApiProvider;
use App\Services\GuardianProvider;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @property mixed title
 * @property mixed api_key
 * @property mixed base_url
 * @property mixed current_page
 */
class Provider extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'base_url', 'api_key', 'current_page'];

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }

    public function getServiceClass(): ?string
    {
        switch ($this->title) {
            case 'guardian':
                return GuardianProvider::class;
            case 'newsapi':
                return NewsApiProvider::class;
            default:
                return null;
        }
    }
}
