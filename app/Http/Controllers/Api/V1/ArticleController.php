<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Article;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    private function getData($articles): array
    {
        return [
            'total' => $articles->total(),
            'articles' => $articles->items(),
            'perPage' => $articles->perPage(),
            'lastPage' => $articles->lastPage(),
            'currentPage' => $articles->currentPage(),
            'nextPageUrl' => $articles->nextPageUrl(),
            'hasMorePages' => $articles->hasMorePages(),
            'previousPageUrl' => $articles->previousPageUrl(),
        ];
    }

    public function search(): JsonResponse
    {
        $articles = Article::query()
            ->scopes(['apiDatesFilter', 'apiSourcesFilter', 'apiCategoriesFilter', 'apiSearchFilter', 'authorFilter'])
            ->paginate();
        $data = $this->getData($articles);
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function source($source): JsonResponse
    {
        $articles = Article::query()
            ->where('source', '=', $source)
            ->scopes(['apiDatesFilter', 'apiCategoriesFilter', 'apiSearchFilter', 'apiAuthorFilter'])
            ->where('source', '=', $source)
            ->paginate();
        $data = $this->getData($articles);
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function category($category): JsonResponse
    {
        $articles = Article::query()
            ->scopes(['apiDatesFilter', 'apiSourcesFilter', 'apiSearchFilter', 'apiAuthorFilter'])
            ->where('category', '=', $category)
            ->paginate();
        $data = $this->getData($articles);
        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function author($author): JsonResponse
    {
        $articles = Article::query()
            ->scopes(['apiDatesFilter', 'apiSourcesFilter', 'apiCategoriesFilter', 'apiSearchFilter'])
            ->where('author', '=', $author)
            ->paginate();
        $data = $this->getData($articles);
        return response()->json(['status' => 'success', 'data' => $data]);
    }
}
