<?php

namespace App\Repositories;

use App\Models\Provider;
use App\Contracts\ProviderRepositoryInterface;

class ProviderRepository implements ProviderRepositoryInterface
{
    public function update(Provider $provider, $current_page)
    {
        $provider->update(['current_page' => $current_page]);
    }
}
