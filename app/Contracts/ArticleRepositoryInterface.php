<?php

namespace App\Contracts;

interface ArticleRepositoryInterface
{
    public function storeOrUpdate(array $data);
}
