<?php

namespace App\Providers;

use App\Models\Provider;
use App\Contracts\NewsProviderInterface;
use Illuminate\Support\ServiceProvider;
use App\Repositories\ArticleRepository;
use App\Contracts\ArticleRepositoryInterface;
use App\Services\{GuardianProvider, NewsApiProvider};

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NewsProviderInterface::class, function ($app, $parameter) {
            if (isset($parameter['title'])) {
                $provider = Provider::query()->where('title', '=', $parameter['title'])->firstOrFail();
                return $this->app->make($provider->getServiceClass());
            }
            return null;
        });

        $this->app->bind(ArticleRepositoryInterface::class, ArticleRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
