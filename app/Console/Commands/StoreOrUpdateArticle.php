<?php

namespace App\Console\Commands;

use App\Models\Provider;
use Illuminate\Console\Command;
use App\Services\ArticleService;

class StoreOrUpdateArticle extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:storeOrUpdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Store new articles or update exists from given providers';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $providers = Provider::query()
            ->orderBy('current_page')
            ->get();

        $bar = $this->output->createProgressBar($providers->count());

        foreach ($providers as $provider) {
            $this->info($provider->title);
            $articleService = new ArticleService($provider);
            $articleService->fetchAndStoreOrUpdateArticles();
            $bar->advance();
        }

        $bar->finish();
        $this->info('finish storing');
    }
}
