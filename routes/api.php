<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\ArticleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('V1')->group(function () {
    Route::prefix('articles')->group(function () {
        Route::get('search', [ArticleController::class, 'search']);
        Route::get('source/{source}', [ArticleController::class, 'source']);
        Route::get('category/{category}', [ArticleController::class, 'category']);
        Route::get('author/{author}', [ArticleController::class, 'author']);
    });
});
